import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.less']
})
export class HeroComponent implements OnInit {

  constructor() { }

  @Input() title:string = "";
  @Input() subtitle:string = "";

  ngOnInit(): void {
  }

}
