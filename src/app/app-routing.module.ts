import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageHomeComponent } from './page-home/page-home.component';
import { PageSettingsComponent } from './page-settings/page-settings.component';
import { PageLoginComponent } from './page-login/page-login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PageAdminParseScheduleComponent } from './page-admin-parse-schedule/page-admin-parse-schedule.component';
import { PageAdminLoginComponent } from './page-admin-login/page-admin-login.component';
import { PageAdminIndexComponent } from './page-admin-index/page-admin-index.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: PageHomeComponent },
  { path: 'settings', component: PageSettingsComponent },
  { path: 'login', component: PageLoginComponent },
  { path: 'admin', component: PageAdminIndexComponent },
  { path: 'admin/login', component: PageAdminLoginComponent },
  { path: 'admin/parse', component: PageAdminParseScheduleComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
