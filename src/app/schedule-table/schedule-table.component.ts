import { Component, OnInit } from '@angular/core';
import { ScheduleService } from '../schedule.service';
import { LocalStorageSettingsService } from '../local-storage-settings.service';

@Component({
  selector: 'app-schedule-table',
  templateUrl: './schedule-table.component.html',
  styleUrls: ['./schedule-table.component.less']
})
export class ScheduleTableComponent implements OnInit {

  schedule: any[];
  filtersVisible = false;

  group: number;
  searchTerm: string;

  constructor(private service: ScheduleService, private settingsService: LocalStorageSettingsService) {
    this.group = this.settingsService.getGroup();
    this.searchTerm = "";
  }

  ngOnInit(): void {
  
    this.updateSchedule();
  }

  isThemeLight() {
    return this.settingsService.getTheme() === "light";
  }

  isClassToday(subject) {
    return new Date().getDay() === subject.day;
  }

  toggleFilters() {
    this.filtersVisible = !this.filtersVisible;
  }

  setGroup(group) {
    this.group = group.value; 
    this.updateSchedule();
  }

  setSearch(input) {
    this.searchTerm = input;
    this.updateSchedule();
  }

  updateSchedule() {
    this.schedule = this.service.getAll().filter(x => JSON.stringify(x).includes(this.searchTerm) && (x.group === 0 || x.group === this.group || this.group === 0));
  }

}
