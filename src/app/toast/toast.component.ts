import { Component, OnInit } from '@angular/core';
import { ToastService } from '../toast.service';

@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.less']
})
export class ToastComponent implements OnInit {

  active = false;
  data: any = {text: ""};

  constructor(private service: ToastService) {
    const displayToast = () => {
      this.display();
    }

    //https://stackoverflow.com/questions/40788458/how-to-call-component-method-from-service-angular2
    this.service.componentMethodCalled$.subscribe(displayToast);
  }

  ngOnInit(): void {
  }

  display() {
    this.data = this.service.getData();
    this.active = true;

    setTimeout(() => {
      this.close();
    }, this.data.duration ? this.data.duration : 4000);
  }

  close() {
    this.active = false;
  }
}
