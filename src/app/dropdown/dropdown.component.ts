import { Component, OnInit, Input, Output, EventEmitter, HostListener, ElementRef } from '@angular/core';
import { LocalStorageSettingsService } from '../local-storage-settings.service';

@Component({
  selector: 'dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.less']
})
export class DropdownComponent implements OnInit {

  activeItem: {text: string, value: any};
  @Input() default: number = 0;
  @Input() items: {text: string, value: any}[];
  @Output() onItemClick = new EventEmitter<{text: string, value: any}>();

  constructor(private service: LocalStorageSettingsService, private eRef: ElementRef) { }

  active: boolean = false;

  ngOnInit(): void {

    this.activeItem = this.items[this.default];
  }

  openDropdown() {
    this.active = true;
  }

  closeDropdown() {
    this.active = false;
  }

  toggleDropdown() {
    this.active = !this.active;
  }

  //https://stackoverflow.com/questions/40107008/detect-click-outside-angular-component
  @HostListener('document:click', ['$event'])
  clickout(event) {
    if(!this.eRef.nativeElement.contains(event.target)) {
      this.closeDropdown();
    }
  }

  isThemeLight() {
    return this.service.getTheme() === "light";
  }

  onClick(item: {text: string, value: any}) {
    this.onItemClick.emit(item);
    this.activeItem = item;

    this.closeDropdown();
  }

}
