import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageSettingsService {

  settings = {
    theme: 'light',
    displayNext: true,
    group: 3,
  }

  constructor() {
    this.load();
  }

  getTheme() {
    return this.settings.theme;
  }

  setTheme(value) {
    this.set('theme', value);

    if(value === 'light') {
      document.documentElement.classList.add("light");
    } else {
      document.documentElement.classList.remove("light");
    }
  }

  getDisplayNext() {
    return this.settings.displayNext;
  }

  setDisplayNext(value) {
    this.set('displayNext', value);
  }

  getGroup() {
    return this.settings.group;
  }

  setGroup(value) {
    this.set('group', value);
  }

  get(key: string) {
    return this.settings[key];
  }

  set(key, value) {
    this.settings[key] = value;
    this.save();
  }
  
  save() {
    window.localStorage.setItem("settings", JSON.stringify(this.settings));
  }

  load() {
    const settingsFromLocalStorage = JSON.parse(window.localStorage.getItem("settings"));

    if(settingsFromLocalStorage)
      this.settings = settingsFromLocalStorage;
  }
}
