import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import NetlifyIdentity from 'netlify-identity-widget';
import { Router } from '@angular/router';

@Component({
  selector: 'app-page-admin-login',
  templateUrl: './page-admin-login.component.html',
  styleUrls: ['./page-admin-login.component.less']
})
export class PageAdminLoginComponent implements OnInit {

  constructor(private service: AuthenticationService, private router: Router ) { }

  ngOnInit(): void {
  }

  login() {
    this.service.authenticate(() => {
      this.router.navigate(['/admin']);
    });
  }

  signup() {
    this.service.signup();
  }

}
