import { Injectable } from '@angular/core';
import { ToastService } from './toast.service';

@Injectable({
  providedIn: 'root'
})
export class ParserService {

  daysOfWeekBulgarian = ["Неделя", "Понеделник", "Вторник", "Сряда", "Четвъртък", "Петък", "Събота"];

  constructor(private toast: ToastService) { }

  parse( html: string) {
    // Create a virtual DOM element for DOM parsing purposes
    const root = document.createElement('html');

    // Put the HTML string inside the root DOM element, automatically parsing it in the process
    root.innerHTML = html;

    // Find the schedule table in the DOM. Look for a <table> with an id that contains "grzan"
    let scheduleTable = this.findScheduleTableInDOM(root);

    // Make sure the table is not null, and if it is, display a friendly error message then exit
    if(!this.validate(scheduleTable)) return;
    
    // Get all <tr> elements from the schedule table
    const entries = scheduleTable.getElementsByTagName('tr');

    let parsedSchedule = []; 
    for(let i = 1; i < entries.length; i++) {
      let entry = entries[i];

      let subject:any = {};


      subject.day = this.daysOfWeekBulgarian.findIndex(x => x === entry.childNodes[1].textContent);
      subject.hour = parseInt(entry.childNodes[2].textContent.split('.'));
      subject.week = this.getWeekNo(entry.childNodes[3].textContent);
      subject.group = this.getGroup(entry.childNodes[4].innerHTML);
      subject.room = entry.childNodes[5].textContent;
      subject.subject = entry.childNodes[8].textContent;
      subject.lecturer = entry.childNodes[9].textContent; 

      parsedSchedule.push(subject);
    }

    return parsedSchedule;
  }

  validate(scheduleTable) {
    if(!scheduleTable) {
      console.error('The given HTML string does not seem to contain a schedule table. Please copy the entire Page Source for https://est.uni-vt.bg/student/spr/grzan.aspx');
      this.toast.display(
        {
          text: "Дадения текст не изглежда да съдържа график на занятията. Моля копирайте целия Page Source (източник на страницата) от https://est.uni-vt.bg/student/spr/grzan.aspx",
          color: "is-danger"
        }
      )

      return false;
    }

    return true;
  }

  getWeekNo(week) {
    let weekNumber = week.split(",")[0];

    let parsedWeekNo = parseInt(weekNumber);

    // parsedWeekNo is NaN when the `week` is "всяка", so return 0 instead
    if(isNaN(parsedWeekNo)) return 0;
    return parsedWeekNo;
  }

  getGroup(group) {
    if(group === "&nbsp;") return 0;
    else {
      return parseInt(group.split(" ")[0]);
    }
  }

  findScheduleTableInDOM(root) {
    const tables = root.getElementsByTagName('table');
    for(let i = 0; i < tables.length; i++) {
      if(tables[i].id.includes('grzan')) {
        return tables[i];
        break;
      }
    }
    return null;
  }
}