import { Component, OnInit } from '@angular/core';
import { LocalStorageSettingsService } from '../local-storage-settings.service';
import { NextClassService } from '../next-class.service';
import { ScheduleService } from '../schedule.service';

@Component({
  selector: 'app-next-class',
  templateUrl: './next-class.component.html',
  styleUrls: ['./next-class.component.less']
})
export class NextClassComponent implements OnInit {

  constructor(private service: NextClassService, private settingsService: LocalStorageSettingsService) { }

  nextClass: any;
  afterThat: any;

  ngOnInit(): void {
    let nextClasses = this.service.localizeTime(this.service.getNext( this.settingsService.getGroup() ));
    this.nextClass = nextClasses[0];
    this.afterThat = nextClasses[1];
  }

  isThemeLight() {
    return this.settingsService.getTheme() === "light";
  }

  displayNextClass() {
    return this.settingsService.getDisplayNext();
  }
}
