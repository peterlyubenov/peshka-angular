import { Injectable } from '@angular/core';
import { ScheduleService } from './schedule.service';
@Injectable({
  providedIn: 'root'
})
export class NextClassService {

  constructor(private service: ScheduleService) { }

  daysOfWeekBulgarian = ["Неделя", "Понеделник", "Вторник", "Сряда", "Четвъртък", "Петък", "Събота"];
  weekOffset = 0;

  getWeek(date) {
    let onejan: any = new Date(date.getFullYear(), 0, 1);
    return Math.ceil((((date - onejan) / 86400000) + onejan.getDay()+1)/7);
  }

  getNext(group?: number) {

    if(!group) group = 3

    const date = new Date();
    const day = date.getDay();
    const hour = Math.floor(date.getHours() / 2) * 2;

    const schedule = this.service.getAll();
    const week = (this.getWeek(date) + this.weekOffset) % 2 + 1;

    // Find the class that is either taking place right now if there is one, or the next one
    let next = this.findClass(schedule, week, group, this.service.encode(day, hour));

    // If no class found, loop back to the beginning of the week and find the first class
    if(!next) next = this.findClass(schedule, (week + 1) % 2, group, 0);

    // Find the class after next
    let afterThat = this.findClass(schedule, week, group, next.classno+1);

    if(!afterThat) {
      afterThat = this.findClass(schedule, (week + 1) % 2, group, 0)
    }

    return [next, afterThat];
  }

  findClass(schedule, week, group, classno) {
    return schedule.find(
      (x) => {
        if(x.week === week || x.week === 0) {
          if(group === 0 || x.group === group || x.group === 0) {
            if(x.classno >= classno) {
              return true;
            }
          }
        }
        return false;
      });
  }

  /**
   *  Returns a friendly string of the time of next class e.g 12:15 or Утре в 8:15 or Днес в 16:15 
   * @param next The Next Class object to localize
   * @param afterThat The class after next
   */
  localizeTime(next) {
    let date = new Date();
    if(next[0].day != date.getDay() || next[1].day != date.getDay()) {
      this.localize(next[0], date);
      this.localize(next[1], date);
    }
    else {
      next[0].localizedHour = next[0].hour;
      next[1].localizedHour = next[1].hour;
    }

    return [next[0], next[1]];
  }

  localize(next, date) {
    if(next.day === date.getDay()) next.localizedHour = "Днес в " + next.hour
    else if(next.day === date.getDay() + 1) next.localizedHour = "Утре в " + next.hour
    else {
      next.localizedHour = this.daysOfWeekBulgarian[next.day] + " в " + next.hour
    }

    // console.log(date.getDay(), next.day);
  }

}
