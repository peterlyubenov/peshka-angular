// https://stackoverflow.com/questions/12709074/how-do-you-explicitly-set-a-new-property-on-window-in-typescript
declare global {
  interface Window { netlifyIdentity: any; }
}

import { Component } from '@angular/core';
import { LocalStorageSettingsService } from './local-storage-settings.service';
import netlifyIdentity from 'netlify-identity-widget';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  constructor(private settingsService: LocalStorageSettingsService) {
    let theme = settingsService.get("theme");

    if(!theme) {
      settingsService.set("theme", "light");
      theme = "light";
    }

    if(theme === "light") {
      document.documentElement.classList.add("light");
    }

    window.netlifyIdentity = netlifyIdentity;
    netlifyIdentity.init();
  }
}
