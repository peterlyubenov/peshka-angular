import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageHomeComponent } from './page-home/page-home.component';
import { PageSettingsComponent } from './page-settings/page-settings.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PageLoginComponent } from './page-login/page-login.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HeroComponent } from './hero/hero.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { ToastComponent } from './toast/toast.component';
import { NextClassComponent } from './next-class/next-class.component';
import { PageAdminParseScheduleComponent } from './page-admin-parse-schedule/page-admin-parse-schedule.component';
import { ScheduleTableComponent } from './schedule-table/schedule-table.component';
import { PageAdminLoginComponent } from './page-admin-login/page-admin-login.component';
import { PageAdminIndexComponent } from './page-admin-index/page-admin-index.component';

@NgModule({
  declarations: [
    AppComponent,
    PageHomeComponent,
    PageSettingsComponent,
    PageNotFoundComponent,
    PageLoginComponent,
    NavbarComponent,
    HeroComponent,
    DropdownComponent,
    ToastComponent,
    NextClassComponent,
    PageAdminParseScheduleComponent,
    ScheduleTableComponent,
    PageAdminLoginComponent,
    PageAdminIndexComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
