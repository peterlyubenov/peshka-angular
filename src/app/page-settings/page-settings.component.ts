import { Component, OnInit } from '@angular/core';
import { LocalStorageSettingsService } from '../local-storage-settings.service';
import { ToastService } from '../toast.service';

@Component({
  selector: 'app-page-settings',
  templateUrl: './page-settings.component.html',
  styleUrls: ['./page-settings.component.less']
})
export class PageSettingsComponent implements OnInit {

  constructor(private service: LocalStorageSettingsService, private toastService: ToastService) { }

  ngOnInit(): void {
  }

  toggleDisplayNext() {
    this.service.setDisplayNext(!this.service.getDisplayNext());
    this.showSuccessfulSaveNotification();
  }

  isDisplayNext() {
    return this.service.getDisplayNext();
  }

  isThemeLight() {
    return this.service.getTheme() === "light";
  }

  toggleTheme() {
    this.service.setTheme(this.service.getTheme() === "light" ? "dark" : "light");
    this.showSuccessfulSaveNotification();
  }

  changeGroup(event) {
    this.service.setGroup(event.value); 
    this.showSuccessfulSaveNotification();
  }

  getGroup() {
    return this.service.getGroup();
  }

  showSuccessfulSaveNotification() {
    this.toastService.display({title: "Успех", text: "Промените бяха успешно запазени", color: "is-success"})
  }
}
