import { Component, OnInit, HostListener, ElementRef } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.less']
})
export class NavbarComponent implements OnInit {

  constructor(private eRef: ElementRef) { }

  mobileIsActive: boolean = false;

  ngOnInit(): void {
  }

  toggleNavar() {
    this.mobileIsActive = !this.mobileIsActive;
  }

  clickLink() {
    this.mobileIsActive = false;
  }

  @HostListener('document:click', ['$event'])
  clickout(event) {
    if(!this.eRef.nativeElement.contains(event.target)) {
      this.mobileIsActive = false;
    }
  }

}
