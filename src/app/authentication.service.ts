import { Injectable } from '@angular/core';
import netlifyIdentity from 'netlify-identity-widget';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  public isAuthenticated = false;
  public user = null;

  constructor() { }

  authenticate(callback) {
    netlifyIdentity.open();
    netlifyIdentity.on('login', user => {
      this.isAuthenticated = true;
      this.user = user;
      callback(user);
    })
  }

  signup() {
    netlifyIdentity.open('signup');
  }  

  signout(callback) {
    netlifyIdentity.logout();
    netlifyIdentity.on('logout', () => {
      this.isAuthenticated = false;
      this.user = null;
      callback();
    })
  }
}
