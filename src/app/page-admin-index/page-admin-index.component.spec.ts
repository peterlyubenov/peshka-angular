import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageAdminIndexComponent } from './page-admin-index.component';

describe('PageAdminIndexComponent', () => {
  let component: PageAdminIndexComponent;
  let fixture: ComponentFixture<PageAdminIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageAdminIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageAdminIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
