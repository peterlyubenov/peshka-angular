import { Component, OnInit } from '@angular/core';
import netlifyIdentity from 'netlify-identity-widget';
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-page-admin-index',
  templateUrl: './page-admin-index.component.html',
  styleUrls: ['./page-admin-index.component.less']
})
export class PageAdminIndexComponent implements OnInit {

  constructor(private authenticationService: AuthenticationService, private router: Router) { }

  ngOnInit(): void {

    if(! netlifyIdentity.currentUser()) {
      this.router.navigate(['/admin', 'login']);
    }
  }

}
