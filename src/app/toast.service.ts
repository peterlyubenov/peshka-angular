import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor() { }

  toastData: {text: string, duration?: number, title?: string, color?:string};

  getData() {
    return this.toastData;
  }

  // https://stackoverflow.com/questions/40788458/how-to-call-component-method-from-service-angular2
  private componentMethodCallSource = new Subject<any>();
  componentMethodCalled$ = this.componentMethodCallSource.asObservable();

  display(toastData: {text: string, duration?: number, title?: string, color?:string}) {
    this.toastData = toastData;
    this.componentMethodCallSource.next();
  }
}
