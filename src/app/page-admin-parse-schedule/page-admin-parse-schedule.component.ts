import { Component, OnInit } from '@angular/core';
import { ParserService } from '../parser.service';
import { LocalStorageSettingsService } from '../local-storage-settings.service';
import { ScheduleService } from '../schedule.service';
import { ToastService } from '../toast.service';

@Component({
  selector: 'app-page-admin-parse-schedule',
  templateUrl: './page-admin-parse-schedule.component.html',
  styleUrls: ['./page-admin-parse-schedule.component.less']
})
export class PageAdminParseScheduleComponent implements OnInit { 

  constructor(private service: ParserService, private settingsService: LocalStorageSettingsService, private scheduleService: ScheduleService, private toastService: ToastService ) { }
 
  ngOnInit(): void {
  }

  isLightTheme() {
    return this.settingsService.getTheme() === "light";
  }

  parseHtml( event, html ) {
    event.preventDefault();

    this.scheduleService.setSchedule(this.service.parse(html));
    
    this.toastService.display({text: "Програмата е актуализирана успешно", color: "is-success"});
  }

}
