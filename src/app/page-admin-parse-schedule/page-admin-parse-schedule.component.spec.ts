import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageAdminParseScheduleComponent } from './page-admin-parse-schedule.component';

describe('PageAdminParseScheduleComponent', () => {
  let component: PageAdminParseScheduleComponent;
  let fixture: ComponentFixture<PageAdminParseScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageAdminParseScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageAdminParseScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
