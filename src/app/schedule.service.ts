import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  schedule: any[] = [
    {
      day: 1,
      hour: 8,
      week: 0,
      group: 1,
      room: '703',
      subject: 'Информационни системи за управление на софтуерни проекти',
      lecturer: 'Калоян Любенов Здравков'
    },
    {
      day: 1,
      hour: 10,
      week: 0,
      group: 2,
      room: '703',
      subject: 'Информационни системи за управление на софтуерни проекти',
      lecturer: 'Калоян Любенов Здравков'
    },
    {
      day: 1,
      hour: 12,
      week: 1,
      group: 1,
      room: '607',
      subject: 'Интернет базиранa свързаност на устройства (IoT)',
      lecturer: 'гл.ас. д-р Теодор Ангелов Калушков'
    },
    {
      day: 1,
      hour: 12,
      week: 2,
      group: 2,
      room: '607',
      subject: 'Интернет базиранa свързаност на устройства (IoT)',
      lecturer: 'гл.ас. д-р Теодор Ангелов Калушков'
    },
    {
      day: 1,
      hour: 12,
      week: 0,
      group: 3,
      room: '703',
      subject: 'Информационни системи за управление на софтуерни проекти',
      lecturer: 'Калоян Любенов Здравков'
    },
    {
      day: 1,
      hour: 14,
      week: 1,
      group: 3,
      room: '607',
      subject: 'Интернет базиранa свързаност на устройства (IoT)',
      lecturer: 'гл.ас. д-р Теодор Ангелов Калушков'
    },
    {
      day: 2,
      hour: 8,
      week: 0,
      group: 0,
      room: '506',
      subject: 'Методи за транслация',
      lecturer: 'доц. д-р Юлиана Стоянова Дошкова-Тодорова'
    },
    {
      day: 2,
      hour: 10,
      week: 1,
      group: 0,
      room: '406',
      subject: 'Добиване на данни (data mining)',
      lecturer: 'доц. д-р Цветанка Любомирова Георгиева-Трифонова'
    },
    {
      day: 2,
      hour: 10,
      week: 2,
      group: 0,
      room: '406',
      subject: 'Информационни системи за управление на софтуерни проекти',
      lecturer: 'доц. д-р Цветанка Любомирова Георгиева-Трифонова'
    },
    {
      day: 2,
      hour: 12,
      week: 0,
      group: 3,
      room: '704',
      subject: 'Добиване на данни (data mining)',
      lecturer: 'доц. д-р Цветанка Любомирова Георгиева-Трифонова'
    },
    {
      day: 2,
      hour: 14,
      week: 0,
      group: 2,
      room: '704',
      subject: 'Добиване на данни (data mining)',
      lecturer: 'доц. д-р Цветанка Любомирова Георгиева-Трифонова'
    },
    {
      day: 3,
      hour: 8,
      week: 1,
      group: 0,
      room: '406',
      subject: 'Шумозащитно кодиране',
      lecturer: 'проф. дн Стефка Христова Буюклиева'
    },
    {
      day: 3,
      hour: 10,
      week: 0,
      group: 1,
      room: '704',
      subject: 'Абстракции и преизползваем код в клиентска среда',
      lecturer: 'Йоан Людмилов Иванов'
    },
    {
      day: 3,
      hour: 10,
      week: 1,
      group: 2,
      room: '504',
      subject: 'Шумозащитно кодиране',
      lecturer: 'гл.ас. д-р Мария Стефанова Джумалиева - Стоева'
    },
    {
      day: 3,
      hour: 10,
      week: 2,
      group: 3,
      room: '504',
      subject: 'Шумозащитно кодиране',
      lecturer: 'гл.ас. д-р Мария Стефанова Джумалиева - Стоева'
    },
    {
      day: 3,
      hour: 12,
      week: 1,
      group: 1,
      room: '403',
      subject: 'Шумозащитно кодиране',
      lecturer: 'гл.ас. д-р Мария Стефанова Джумалиева - Стоева'
    },
    {
      day: 3,
      hour: 12,
      week: 0,
      group: 2,
      room: '704',
      subject: 'Абстракции и преизползваем код в клиентска среда',
      lecturer: 'Йоан Людмилов Иванов'
    },
    {
      day: 3,
      hour: 14,
      week: 0,
      group: 1,
      room: '503',
      subject: 'Добиване на данни (data mining)',
      lecturer: 'доц. д-р Цветанка Любомирова Георгиева-Трифонова'
    },
    {
      day: 3,
      hour: 14,
      week: 0,
      group: 3,
      room: '704',
      subject: 'Абстракции и преизползваем код в клиентска среда',
      lecturer: 'Йоан Людмилов Иванов'
    },
    {
      day: 4,
      hour: 10,
      week: 0,
      group: 1,
      room: '706',
      subject: 'Методи за транслация',
      lecturer: 'гл.ас. д-р Силвия Ангелова Върбанова'
    },
    {
      day: 4,
      hour: 12,
      week: 0,
      group: 2,
      room: '706',
      subject: 'Методи за транслация',
      lecturer: 'гл.ас. д-р Силвия Ангелова Върбанова'
    },
    {
      day: 4,
      hour: 14,
      week: 0,
      group: 3,
      room: '706',
      subject: 'Методи за транслация',
      lecturer: 'гл.ас. д-р Силвия Ангелова Върбанова'
    },
    {
      day: 5,
      hour: 8,
      week: 1,
      group: 0,
      room: '306',
      subject: 'Интернет базиранa свързаност на устройства (IoT)',
      lecturer: 'проф. дн Олег Димитров Асенов'
    },
    {
      day: 5,
      hour: 8,
      week: 2,
      group: 0,
      room: '306',
      subject: 'Софтуерни архитектури',
      lecturer: 'гл.ас. д-р Гергана Виолетова Маркова'
    },
    {
      day: 5,
      hour: 10,
      week: 0,
      group: 1,
      room: '606',
      subject: 'Софтуерни архитектури',
      lecturer: 'гл.ас. д-р Гергана Виолетова Маркова'
    },
    {
      day: 5,
      hour: 12,
      week: 0,
      group: 2,
      room: '606',
      subject: 'Софтуерни архитектури',
      lecturer: 'гл.ас. д-р Гергана Виолетова Маркова'
    },
    {
      day: 5,
      hour: 14,
      week: 0,
      group: 3,
      room: '606',
      subject: 'Софтуерни архитектури',
      lecturer: 'гл.ас. д-р Гергана Виолетова Маркова'
    },
    {
      day: 6,
      hour: 14,
      week: 1,
      group: 0,
      room: '402',
      subject: 'Абстракции и преизползваем код в клиентска среда',
      lecturer: 'гл.ас. д-р Венелин Симеонов Монев'
    }
  ]

  constructor() {
    this.setClassNoForSchedule();
  }

  setClassNoForSchedule() {
    for (let subject of this.schedule) {
      if(!subject.classno) {
        subject.classno = this.encode(subject.day, subject.hour);
      }
    }
  }

  getAll() {
    return this.schedule;
  }

  /**
   *  Assigns a unique numeric value to the given combination of day and hour of the week
   * @param day The numeric representation of the day of the week e.g Monday => 1, Wednesday = 3
   * @param time THe numeric representation of a class, rounded down to the nearest even integer e.g 14:15 => 14, 17:10 => 16
   */
  encode(day, time) {
    return day*12 + time/2;
  }

  /**
   *  Decodes the unique numeric value of a class back into day of week and hours rounded down to nearest even integer 
   * @param classno The numeric value of the class 
   */
  decode(classno) {
    return {
      day: Math.floor(classno/12) * 2,
      time: classno % 12
    }
  }

  /**
   *  Overwrites the existing schedule with a new one
   * @param schedule An array of all subjects in the schedule
   */
  setSchedule(schedule) {
    this.schedule = schedule;
    this.setClassNoForSchedule();
  }
}
